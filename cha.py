#!/usr/bin/env python3

#CHA:xhrade08

import sys
import re
import os
import codecs
from xml.etree.ElementTree import Element, SubElement, tostring
import xml.dom.minidom

# navratove kody namisto magic numbers
SUCCESS = 0
ERR_PARAMS = 1
ERR_INPUT = 2
ERR_OUTPUT = 3
ERR_INPUT_FORMAT = 4

#pocitadlo parametru
params= {'help' : 0, 'input' : 0, 'output' : 0, 'prettyxml' : 0, 'noinline':0, 'maxpar':0, 'noduplicates':0, 'removewhitespaces':0 }
prettyxml= 0
maxpar = -1
input= -1
output= -1

#projedou se vsechny parametry a inkrementuji se jejich vyskyty  
for arg in sys.argv[1:]:
	if arg in "--help":
		params["help"] += 1
	elif re.match("^--input=",arg):
		params["input"] += 1
		input=arg[8:]
	elif re.match("^--output=",arg):
		params["output"] += 1
		output=arg[9:]
	elif re.match("^--max-par=",arg):
		params["maxpar"] += 1
		try:
			maxpar = int(arg[10:])
		except ValueError:
			sys.stderr.write("Chyba: Parametr %s neni cislo.\n" % arg[10:])
			sys.exit(ERR_PARAMS)
	elif arg in "--pretty-xml":
		params["prettyxml"] += 1
		prettyxml=4
	elif re.match("^--pretty-xml=",arg):
		params["prettyxml"] += 1
		try:
			prettyxml = int(arg[13:])
		except ValueError:
			sys.stderr.write("Chyba: Parametr %s neni cislo.\n" % arg[13:])
			sys.exit(ERR_PARAMS)
	elif arg in "--no-inline":
		params["noinline"] += 1
	elif arg in "--no-duplicates":
		params["noduplicates"] += 1
	elif arg in "--remove-whitespaces":
		params["removewhitespaces"] += 1
	else:
		sys.stderr.write("Chyba: Parametr %s neni podporovan.\n" % arg)
		sys.exit(ERR_PARAMS)

# kontrola duplicity parametru
for k in params.keys():
	if params[k] > 1:
		sys.stderr.write("Chyba: Parametr byl zadan dvakrat.\n")
		sys.exit(ERR_PARAMS)

# samotny beh scriptu
if len(sys.argv)==2 and params["help"]==1:
	print ("CHA - analyza hlavickovych souboru\n",
	"--input=fileordir urcuje vstupni zdrojovy soubor nebo adresar, kdyz neni zadan, zpracuji se vsechny .h soubory v aktualnim adresari a podadesarich\n",
	"--output=file urcuje vystupni soubor, kdyz neni urcen, zapisuje se na standartni vystup\n",
	"--pretty-xml=k kdyz je zadan, tak kazdy novy element bude na novem radku odsazen o k*pocet zanoreni\n",
	"--no-inline ignoruje funkce se specifikatorem inline\n",
	"--max-par=k funkce, ktere maji vice nez k parametru jsou ignorovany\n",
	"--no-duplicates kdyz ma vice funkci v jednom souboru stejny nazev, tak se zpracuje jen prvni, ostatni jsou ignorovany\n",
	"--remove-whitespaces bile znaky jsou transformovany na mezery a prebytecne mezery jsou odstraneny\n")
	sys.exit(SUCCESS)
elif len(sys.argv)!=2 and params["help"]==1:
	sys.stderr.write("Chyba: Parametr --help je vylucny.\n")
	sys.exit(ERR_PARAMS)
else:
	headerfiles = []

	# ziskani vsech vstupnich souboru z parametru --input=
	if input==-1 or input[len(input)-1]=="/":
		directory = './' if input==-1 else input
		# vyhledani vsech souboruv  adresarich a jejich podadresarich
		for dirname, dirnames, filenames in os.walk('.' if input==-1 else input):
			for filename in filenames:
				if filename[len(filename)-2:]==".h":
					path = os.path.join(dirname, filename) 
					if input==-1:
						path = path[2:]
					headerfiles.append(path) 
	else:
		headerfiles.append(input)
		directory='';
	
	# otevreni vystupniho souboru z parametru --output=
	if output!=-1:
		try:
			out_file = codecs.open(output,"w",encoding='utf-8')
		except:
			sys.stderr.write("Chyba: Soubor %s nelze otevrit pro zapis.\n" % output)
			sys.exit(ERR_OUTPUT)

	# korenovy xml element
	xml_root = Element('functions',{'dir':directory})

	# zpracujeme postupne vsechny hlavickove soubory
	for file in headerfiles:

		# otevreni jednoho hlavickoveho souboru
		try:
			in_file = codecs.open(file,"r",encoding='utf-8')
		except:
			sys.stderr.write("Chyba: Soubor %s nelze otevrit.\n" % file)
			sys.exit(ERR_INPUT)

		# ulozeni souboru do promenne text
		text = in_file.read()
		in_file.close()

		# pryc radkove kometare
		text = re.sub(r'\/\/.*','',text)

		# pryc blokove kometare
		text = re.sub(r'\/\*.*?\*\/','',text,0,flags=re.DOTALL)
		
		# pryc direktivy preprocesoru
		text = re.sub(r'\#.*','',text)

		# pryc typedef
		text = re.sub(r'typedef[^;]*;','',text)

		# seznam fci - kvuli duplicite jmen fci
		function_names = []

		# regularni vyraz vyhleda vsechny obycejne funkce
		matches = re.findall('(^\s*|(;|\})\s+)([^;\}]+?(\s+\*+\s*|\s*\*+\s+|\s+))([a-zA-Z0-9_]+)\s*\((.*?)\)\s*',text,flags=re.S)
		for match in matches:
			#print (match)

			# pokud je zadan argument --no-inline tak se inline fce nezpracovava
			if params['noinline']==1 and re.match(".*inline.*",match[2],flags=re.S):
				continue

			# pokud je zadan argument --no-duplicates tak fce s duplitnim jmenem nezpracovava
			if params['noduplicates']==1 and match[4] in function_names:
				continue

			# vytvoreni elementu function
			function = Element('function')
			function.attrib['file']= re.sub("^"+directory,'',file)
			function.attrib['name']=match[4]
			
			# odstraneni bilych znaku okolo navratoveho typu fce 
			rettype = match[2]
			rettype = re.sub('^\s*','',rettype,flags=re.M)
			rettype = re.sub('\s*$','',rettype,flags=re.M)

			# odstraneni bilych znaku z navratoveho typu fce
			if params['removewhitespaces']==1:
				rettype = re.sub('\s+',' ',rettype,flags=re.M)

			function.attrib['rettype']=rettype
		

			# zpracovani vsech argumentu fce
			arg_count = 0
			vararg_count = 0
			arguments = match[5].split(',')
			#print ("argumety:",arguments)
			for argument in arguments:
				# void funkce nema zadne parametry - jde se dal...
				if re.match("\s*void\s*",argument,flags=re.S):
					continue

				# kdyz je argument ... tak se nic nezpracovava
				if re.match("\s*\.\.\.\s*",argument,flags=re.S):
					vararg_count += 1
					continue
					#arg_count += 1
					#param = Element('param')
					#param.attrib['number']=str(arg_count)
					#param.attrib['type']="..."
					#function.append(param)
					#continue
				else:
					arg_count += 1

				# vytvoreni noveho elementu pro parametr
				param = Element('param')
				param.attrib['number']=str(arg_count)

				# odriznuti jmena promenne a bilych znaku kolem
				typematch = re.findall("^(.*?)[A-Za-z0-9_]+\s*$",argument,flags=re.S)
				typee = ""
				if len(typematch)!=0:
					typee = typematch[0]
					typee = re.sub('^\s*','',typee,flags=re.S)
					typee = re.sub('\s*$','',typee,flags=re.S)
				else:
					continue

				# nahrazeni posloupnosti bilych znaku za mezeru
				if params['removewhitespaces']==1:
					typee = re.sub('\s+',' ',typee,flags=re.M)
					typee = re.sub('([A-Za-z0-9_])\s+(\*)','\g<1>\g<2>',typee,flags=re.M)
					typee = re.sub('(\*)\s+([A-Za-z0-9_])','\g<1>\g<2>',typee,flags=re.M)
				
				# nastaveni typu parametru
				param.attrib['type']=typee

				# pridani parametry fce do xml struktury
				function.append(param)
			
			# pokud byl zadan parametr --max-par a fce ma vice parametru nez bylo, zadano, tak se nezpracuje
			if maxpar != -1 and arg_count > maxpar:
				continue

			# pridani jmena funkce do seznamu (kvuli duplicite)
			function_names.append(match[4])

			# vyhodnoceni zda je to funkce s promennym poctem argumentu
			function.attrib['varargs']="yes" if vararg_count>0 else "no"

			# pridani elemntu s danou fci do xml struktury 
			xml_root.append(function)

	# vytvoreni "pretty xml"
	string = tostring(xml_root, 'utf-8')
	reparsed = xml.dom.minidom.parseString(string)
	pretty = reparsed.toprettyxml(indent=" "*prettyxml)


	# uprava xml hlavicky
	if params['prettyxml']==1:
		pretty = re.sub(r'^<.*?>','<?xml version="1.0" encoding="utf-8"?>',pretty)
	else:
		pretty = re.sub(r'^<.*?>','<?xml version="1.0" encoding="utf-8"?>',pretty)
		pretty = re.sub(r'>\n','>',pretty)

	# zapis xml do souboru daneho --output/ na stdout
	if output==-1:
		print (pretty)
	else:
		out_file.write(pretty)
		out_file.close()

	# vraci se navratova hodnota 0
	sys.exit(SUCCESS)
